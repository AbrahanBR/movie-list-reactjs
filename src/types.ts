export interface Movie {
    adult: boolean;
    backdrop_path: string;
    belongs_to_collection: boolean;
    budget: number;
    genres: Array<{
        id: number;
        name: string;
    }>;
    homepage: string;
    id: number;
    imdb_id: string;
    original_language: string;
    original_title: string;
    overview: string;
    popularity: number;
    poster_path: null
    production_companies: Array<any>
    production_countries: Array<any>
    release_date: string;
    revenue: number;
    runtime: number;
    spoken_languages: Array<any>
    status: string;
    tagline: string;
    title: string;
    video: boolean;
    vote_average: number;
    vote_count: number;
}

export interface Serie {
    backdrop_path: string;
    created_by: Array<any>;
    episode_run_time: Array<number>;
    first_air_date: string;
    genres: Array<any>;
    homepage: string;
    id: number;
    in_production: boolean;
    languages: Array<string>;
    last_air_date: string;
    last_episode_to_air: {
        air_date: string;
        episode_number: number; 
        id: number; 
        name: string; 
        overview: string;
        production_code: string;
        season_number: number;
        still_path: string ;
        vote_average: number;
        vote_count: number;
    }
    name: string;
    networks: Array<any>;
    next_episode_to_air?: string;
    number_of_episodes: number;
    number_of_seasons: number;
    origin_country: Array<string>;
    original_language: string;
    original_name: string;
    overview: string;
    popularity: number;
    poster_path?: string;
    production_companies: Array<any>;
    production_countries: Array<any>;
    seasons: Array<any>;
    spoken_languages: Array<any>;
    status: string;
    tagline: string;
    type: string;
    vote_average: number;
    vote_count: number;
}