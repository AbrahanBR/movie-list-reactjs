import { Movie, Serie } from "../types";

export const getMovies = async (location: string, search?: boolean): Promise<Movie | Serie> => {
    const apikey = "3dae3b2892bc6678cbad2a6be0290683"; 
    let toFind: string;
    if(search){
        toFind = "";
    }
    try{
        const movieResponse = await fetch(`https://api.themoviedb.org/3${location}?api_key=${apikey}`);
        const movie = await movieResponse.json();
        return movie;
    }catch(err){
        throw err;
    }
}