import { Movie } from "../../types";

export interface Props {
    searchInformation: string;
    sectionTitle: string;
    showType: string;
}