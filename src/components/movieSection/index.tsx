import React, { useEffect, useRef, useState } from "react";
import { Movie } from "../../types";
import { MovieCard } from "../movieCard";
import Loading from "../shared/loading";
import { Button,  ButtonPosition, Section, SectionTitleStyle, Wrap } from "./style";
import { ReactComponent as ArrowIcon } from "../shared/svg/chevron-right-solid.svg";
import { Props } from "./types";

const MovieSection = (props:Props) => {
    const { searchInformation, sectionTitle, showType } = props;
    const [movies, setMovies] = useState<Array<Movie>>([]);
    let [ isLoading, setIsLoading ] = useState(true)
    const ref:any = useRef();

    useEffect(() => {
        const fetchMovie = async () => {
        //const apikey = process.env.VUE_APP_VMDB;      
        const movieResponse = await fetch(`https://api.themoviedb.org/3${searchInformation}?api_key=3dae3b2892bc6678cbad2a6be0290683`)
        const movie = await movieResponse.json();
        
        setMovies(movie.results);
        setIsLoading(false)
    }
    fetchMovie();
    },[searchInformation]);

        
    const scrollSectionLeft = () => {
        ref.current.scrollLeft -= 850;
    }
    const scrollSectionRight = () => {
        ref.current.scrollLeft += 850;
    }
    
    return (
    <>
    {isLoading ? (
            <Wrap style={{height: 300}}>
                <Loading/>
            </Wrap>
        ) : (
            <Wrap>
                <div style={{width: 1070}}>
                <SectionTitleStyle>{sectionTitle}</SectionTitleStyle>
                </div>
                <Section ref={ref}>
                    {movies.map((movie:Movie)=> (
                        <MovieCard key={`${movie.id}`} receved={movie} showType={showType}/>))
                    }
                    <ButtonPosition>
                        <Button onClick={scrollSectionLeft}>
                            <ArrowIcon style={{
                                MozTransformOrigin: "scaleX(-1)",
                                transform: "scaleX(-1)",
                                WebkitTransform: "scaleX(-1)",
                                width: "1rem"}}/>
                        </Button>
                        <Button onClick={scrollSectionRight}><ArrowIcon style={{width: "1rem"}}/></Button>
                    </ButtonPosition>
                </Section>
            </Wrap>
        )   
    }           
    </>
    )
}

export default MovieSection;