import styled from "styled-components";

export const Section = styled.div`
    display: flex;
    overflow: auto;
    padding: 10px;
    max-width: 1070px;
    gap: 1rem;
    overflow: hidden;
    scroll-behavior: smooth;

`
export const Wrap = styled.div`
    display: flex;
    flex-direction : column;
    align-content: flex-start;
    align-items: center;

    
`

export const SectionTitleStyle = styled.h2`
  margin-bottom: 0;
  border-left: solid #61dafb;
  padding-left: 10px;
`

export const ButtonPosition = styled.div`
    position: absolute;
    display: flex;
    justify-content: space-between;
    width: 1150px;
    margin-left: -40px;
    margin-top: 120px;
`

export const Button = styled.button`
    font-size: 2rem;
    border-radius: 100%;
    border: none;
    width: 40px;
    height: 40px;
    display: flex;
    justify-content: center;
    align-items: center;
    color: #757a84;

    &:hover{
        cursor: pointer;
        color: #0797c4;
    }
`
