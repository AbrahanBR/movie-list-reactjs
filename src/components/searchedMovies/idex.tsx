import React, { useEffect, useState } from "react";
import { useLocation } from "react-router";
import { MovieCard } from "../movieCard";
import Loading from "../shared/loading";

const SearchedMovies = () => {
    const [searchResults, setSearchResults] = useState<any>([])
    let [ isLoading, setIsLoading ] = useState(true);
    const location = useLocation().pathname.slice(8);

    useEffect(() => {
        const searchedMovie = async () => {
            //const apikey = process.env.VUE_APP_VMDB; 
            const searchResponse = await fetch(`https://api.themoviedb.org/3/search/movie?api_key=3dae3b2892bc6678cbad2a6be0290683&query=${location}`)
            const search = await searchResponse.json();

            setSearchResults(search.results)
            setIsLoading(false)
        }
        searchedMovie()
    },[location])
    console.log(searchResults)

    return (
        <>  
            {isLoading ? (
                <div>
                    <Loading/>
                </div> 
            ):(      
                    <div>
                        {//@ts-ignore
                        searchResults.map((movie) => 
                            <MovieCard key={movie.id} receved={movie} showType="/movie/"/>
                        )}
                    </div>
                
            )

            }
        </>
    )
}

export default SearchedMovies;

