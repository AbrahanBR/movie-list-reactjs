import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { Movie } from "../../types";
import Loading from "../shared/loading";
import { ActorCard, ActorName, KnowFor, PerfilImg, Section } from "./style";
import { Props } from "./types";

interface Actor {
    adult: boolean;
    gender: number;
    id: number;
    known_for: Array<Movie>
    known_for_department: string;
    name: string;
    popularity: number;
    profile_path?: string; 

    cast_id: number,
    character: string,
    credit_id: number,
    order: number
}

const ActorMovies = (movies:any) =>  {
    
    return movies.map((movie:any) =>
        <Link key={movie.id} to={`/movie/${movie.id}`}  style={{ textDecoration: "none",color: "#000" }}>
            <KnowFor >{movie.title}</KnowFor>
        </Link>
)}

const ActorSection = (props:Props) => {
    const { apiPath } = props;
    const [actor, setActor] = useState<Array<Actor>>([]);
    let [ isLoading, setIsLoading ] = useState(true)

    const isActor = (teste:any):teste is Actor => {
        return !!teste?.title;
    }

    useEffect(() => {
        const fetchActor = async () => {
            //const apikey = process.env.VUE_APP_VMDB;      
            const response = await fetch(`https://api.themoviedb.org/3${apiPath}?api_key=3dae3b2892bc6678cbad2a6be0290683`)
            const actors = await response.json();
            
            setActor(actors.results || actors.cast);
            setIsLoading(false)
        }

        fetchActor();
    },[]);
    
    return ( 
        <>
        {isLoading ? (
            (
                <div>
                    <Loading/>
                </div>
            )
        ):(
            <div style={{display: "flex", justifyContent: "center"}}>
                <Section>
                    {actor?.map((actor) => 
                        /* @ts-ignore */
                        <ActorCard key={actor.id }>
                            <PerfilImg src={`https://www.themoviedb.org/t/p/w138_and_h175_face${actor.profile_path}`}/>
                            <ActorName>{actor.name }</ActorName>
                            {actor.known_for ? ActorMovies(actor.known_for) : <KnowFor >{actor.character}</KnowFor>}
                            
                        </ActorCard>
                    )}
                    
                </Section>
             </div>
        )}
            
        </>
    )
}

export default ActorSection;