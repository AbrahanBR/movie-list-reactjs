import React from "react";
import styled from "styled-components";

export const Section = styled.ul`
    list-style-type: none;
    padding: 0;
    max-width: 1070px;
    display: flex;
    overflow: auto;
`

export const ActorCard = styled.li`
    border: 1px #c6c2c2 solid;
    margin-left: 10px;
    border-radius: 5px;
    width: 138px;
    &:hover{
        cursor: pointer;
    }
`

export const PerfilImg = styled.img`
    width: 138px;
    height: 175px;
`

export const ActorName = styled.h4`
    margin: 0 10px;
`

export const KnowFor = styled.h6`
    margin: 0 10px;
    font-weight: 600;

    &:hover{
        cursor: pointer;
        color: #6f94c2;
        transition-duration: 0.4s;
        transform: scale(1.1);
    }
`


