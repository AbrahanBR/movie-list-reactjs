import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { useLocation } from "react-router";
import { Card, MovieTitle, Poster, Rating } from "./style";
import { ReactComponent as StarIcon} from '../shared/svg/star-solid.svg';
import { isMovie, Props } from "./types";

export const MovieCard = (props:Props) => {
    const { receved, showType } = props;
    //const location = useLocation().pathname.replaceAll(/^[A-Z]+/gi, "");
    const name = isMovie(receved) ? receved.title : receved.name
    return (
        <>
        <Link to={`${showType}${receved.id}`} style={{ textDecoration: "none",color: "#000" }}>
            <Card>
                <Poster src={`https://image.tmdb.org/t/p/w200${receved.poster_path}`}  alt={`Banner of ${name}`}/>
                <Rating value={receved.vote_average}> <StarIcon style={{width: "1rem"}}/><b> {receved.vote_average}</b></Rating>
                
                <MovieTitle>{name}</MovieTitle>
            </Card>
        </Link>
        </>
    )
}