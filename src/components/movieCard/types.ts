import { Movie, Serie } from "../../types";

export const isMovie = (teste:any):teste is Movie => {
    return !!teste?.title;
}
export interface Props {
    receved: Movie | Serie;
    showType: string;
}