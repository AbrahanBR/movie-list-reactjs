import styled from "styled-components";
interface RatingValue {
    value: number;
}
const recevedRatingValue = ({value}:RatingValue) => {
    if(value >= 7){
        return "#41b883"
    }else if(value >= 5){
        return "#fcda31"
    }else {
        return "#db0306"
    } 
};

export const Card = styled.div` 
    margin-right: 10px;
    &:hover {
      color: #5d8bbc;
    }
`

export const MovieTitle =  styled.h4`
    margin: 0;
  `
/*
export const Link = syled.a `
    text-decoration: none;
    color: #000;
`
*/

export const Rating = styled.span`
    color: ${recevedRatingValue};
`


export const Poster = styled.img `
    top: 0;
    left: 0;
    width: 180px;
    height: 258px;
    border-radius: 5px;

    &:hover {
        cursor: pointer;
        transition-duration: 0.6s;
        transform: scale(1.1);
        box-shadow: 10px 5px 5px #aca9a970;
    }
`