import styled from "styled-components";
import logo from './logo.svg';

interface Position{
    position: string;
}

const recevedPosition = ({position}:Position) => position;

export const HeaderHtml = styled.header`
    background-image: linear-gradient(to right, #032541 , #07bbd7, #19d0b3);
    height: 50px;
    top: ${recevedPosition};

    display: flex;
    justify-content: space-around;
    align-items: center;
    width: 100%;
    position: fixed;

    transition: top 0.2s linear;

`

export const Logo = styled.img`
    display: flex;
    width: 100px;
`

export const InputDiv = styled.div `
    display: flex;
    margin-left: 5px;
    height: 30px;
`;
export const SearchInput = styled.input`
    border: none;
    outline: none;
    padding-left: 15px;
    border-top-left-radius: 50px;
    border-bottom-left-radius: 50px;
    width: 400px;
`;
export const SearchButton = styled.button`
    border: none;
    border-top-right-radius: 50px;
    border-bottom-right-radius: 50px;
    padding: 20px;
    height: 40px;
    padding-top: 5px;
    padding-bottom: 5px;
    height: 30px;
    color: #18cfb6;
    background-color: #fff;
    font-size: initial;
    font-weight: bold;

    &:hover{
        cursor: pointer;
        background: linear-gradient(to right , #c0fecf 0%, #1ed5a9 100%);
        color: #fff;
    }
`;