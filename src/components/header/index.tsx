import React, { useEffect, useState } from "react";
import logo from './logo.svg';
import { ReactComponent as SearchIcon} from '../shared/svg/search-solid.svg';

import { Link } from "react-router-dom";
import { InputDiv, SearchButton, SearchInput, HeaderHtml, Logo } from "./style";

const Header = () => {
    const [search, setSearch] = useState("");
    let [scroll, setScroll] = useState(window.scrollY);
    let [postion, setPostion] = useState("0px");
    let [lastScroll, setLastScroll] = useState(0);
    
    
    
    useEffect(() => {
        const teste = () => {
            setScroll(window.scrollY);
                if(lastScroll > scroll) {
                    setPostion("0");
                }else {
                    setPostion("-50px");
                }
            setLastScroll(window.scrollY);
        }

        window.addEventListener("scroll", teste);  
        return () => {
            window.removeEventListener("scroll",teste); 
        };
    },[scroll])

    
   
    return(   
            <HeaderHtml position={postion}>
                <Link to="home"><Logo src={logo}></Logo></Link>

                <InputDiv>
                    <SearchInput type="text" value={search} onChange={(e) => {setSearch(e.target.value)}} ></SearchInput>
                    <Link to={`/search/${search}`} >
                        <SearchButton> <SearchIcon style={{height:20}}/> </SearchButton>
                    </Link>
                </InputDiv>
            </HeaderHtml>
        )
}

export default Header;