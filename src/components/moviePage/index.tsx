import React, { useEffect, useState } from "react";
import { useLocation } from "react-router";
import { Movie, Serie } from "../../types";
import { Poster } from "../movieCard/style";
import { Background, Genre, Genres, LeftDiv, Main, Overview, RightDiv, Tagline, Title } from "./style";
import { ReactComponent as StarIcon} from '../shared/svg//star-solid.svg';
import { Rating } from "../movieCard/style";
import MovieSection from "../movieSection";
import { getMovies } from "../../services/api";
import ActorSection from "../actorSection";


const MoviePage = () => {
    const [recevedMovie, setMovie]  = useState<Movie | Serie>();
    let [ isLoading, setIsLoading ] = useState(true);
    const location = useLocation().pathname;

    useEffect(() => {
        
        const fetchMovie = async () => {
            //const apikey = process.env.VUE_APP_VMDB; 
            const movieResponse = await fetch(`https://api.themoviedb.org/3${location}?api_key=3dae3b2892bc6678cbad2a6be0290683`)
            const movie = await movieResponse.json();
        
            document.title = movie.title || movie.name;
            
            setMovie(movie);
            setIsLoading(false);
            //console.log(getMovies(location));
        }
        fetchMovie();
    },[location]);
    
    return (
        <>
            <Background style={{ backgroundImage:` radial-gradient(circle, rgba(0, 0, 0, 0.5) 0%, rgb(0, 0, 0) 180%), url("https://image.tmdb.org/t/p/w1280${recevedMovie?.backdrop_path}")`}}>
                <Main>

                    <LeftDiv>
                    <Poster src={`https://image.tmdb.org/t/p/w200${recevedMovie?.poster_path}`} />
                    <Rating value={recevedMovie?.vote_average || 0}> <StarIcon style={{width: "1rem"}}/><b> {recevedMovie?.vote_average}</b></Rating>
                    <Genres>
                        {
                            recevedMovie?.genres.map((genre) =>  <Genre>{genre.name}</Genre> )
                        }
                    </Genres>
                    </LeftDiv>

                    <RightDiv>
                        {/*@ts-ignore */}
                        <Title>{recevedMovie?.title || recevedMovie?.name }</Title>
                        <Tagline>{`"${recevedMovie?.tagline}"`}</Tagline>
                        <Overview>{recevedMovie?.overview}</Overview>
                    </RightDiv>

                </Main>
            </Background>

            

            {isLoading ? (
                <div>
                    <p>Loading ...</p>
                </div>   
            ) : (
                <>
                    <ActorSection apiPath={`/movie/${recevedMovie?.id}/credits`}/>
                    <MovieSection sectionTitle="Recomendations" searchInformation={`${location}/recommendations`} showType={`${location.replaceAll(/[0-9]+/g, "")}`}/>
                    <MovieSection sectionTitle="Similar Movies" searchInformation={`${location}/similar`} showType={location.replaceAll(/[0-9]+/g, "")}/>
                </>
            )}
        </>
    )
}

export default MoviePage;