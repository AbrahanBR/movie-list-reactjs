import styled from "styled-components";


export const Background = styled.div `
    background-image: radial-gradient(circle, rgba(0, 0, 0, 0.5) 0%, rgb(0, 0, 0) 100%);
    height: 900px;
    background-repeat: no-repeat;
    background-size: cover;
    color: #fff;

    display: flex;
    justify-content: center;
    align-items: center;
`

export const Poster = styled.img ``

export const Main = styled.div `
    display: flex;
    justify-content: space-around;
    
    width: 1070px;
`

export const Overview =  styled.p `
    font-size: 18px;
    line-height: 30px;
`

export const Title =  styled.h1 `
    font-size: 50px;
    text-shadow: #fff 0 0 5px;
    margin-bottom: 0;
`

export const Tagline =  styled.h4 `
    margin-top: 0;
    font-weight: 100;
    font-style: italic;
`

export const Genres =  styled.ul `
    list-style: none;
    padding: 0;
`
export const Genre =  styled.li`
    padding: 5px;
    font-size: 20px;

    &:hover {
      cursor: pointer;
      color: #41b883;
    }
`

export const MovieSectionDiv =  styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
`


export const LeftDiv = styled.div `
    width: 200px;
`
export const RightDiv = styled.div `
    width: 800px;
`


