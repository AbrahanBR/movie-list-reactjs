import React, { useEffect, useState } from "react";
import ActorSection from "../actorSection";
import MovieSection from "../movieSection";
import { Wrap } from "../movieSection/style";
import Trending from "../trending";

const Home = () => {
    useEffect(() => {
        document.title = "Movie App Home"    
    },[]);
        
    return (
    <>  
        <Wrap>
            <Trending />
        </Wrap>
        <MovieSection sectionTitle="Movies Now Playng" searchInformation="/movie/now_playing" showType="/movie/"/>
        <MovieSection sectionTitle="Top Rated Movies" searchInformation="/movie/top_rated" showType="/movie/"/>
        <MovieSection sectionTitle="Popular Movies" searchInformation="/movie/popular" showType="/movie/"/>
        <MovieSection sectionTitle="TV Shows" searchInformation="/tv/popular" showType="/tv/"/>

        <ActorSection apiPath="/person/popular"/>

    </>       
    )
}

export default Home;