import styled from "styled-components";

export const Background = styled.div`
    transform: translate3d(0
        ,0);
    left: 0;
    width: 1070px;
    height: 500px;
    background-size: cover;;
    background-repeat: no-repeat;
`

export const TrendingButton = styled.button `
    padding: 6px;
    margin-right: 10px;
    border: none;
    border-radius: 100%;

    &:hover{
        cursor: pointer;
    }
`

export const TrendinWrap = styled.div `
    max-width: 1070px; 
    height: 300;
    overflow: hidden;
    display: flex;
    flex-direction: row;
    scroll-behavior: smooth;
`