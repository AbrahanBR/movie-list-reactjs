
import React, { useEffect, useRef, useState } from "react";
import { Movie, Serie } from "../../types";
import { Title } from "../moviePage/style";
import { Wrap } from "../movieSection/style";
import { Background, TrendingButton } from "./style";


const Trending = () => { 
    const [tredings, setTredings] = useState<Array<Movie | Serie>>([]);
    let [ isLoading, setIsLoading ] = useState(true)
    const ref:any = useRef();

    const teste: Array<number> = [];
    let [contagem, setContagem] = useState(0);
    let delay:any;

    const createDelay = () => {
        delay = !delay &&  setInterval(() => { 
            if(contagem * 1070 === 20330){
                setContagem(0);
            }else {
                setContagem( contagem + 1);
            }
            scrollSection(contagem * 1070) 
        }, 5000);
    }

    useEffect(() => {
        createDelay();
        return  () => { clearInterval(delay) }
    },[contagem])

    useEffect(() => {
        const fetchMovie = async () => {
            //const apikey = process.env.VUE_APP_VMDB;      
            const movieResponse = await fetch(`https://api.themoviedb.org/3/trending/all/day?api_key=3dae3b2892bc6678cbad2a6be0290683`);
            const movie = await movieResponse.json();
            
            setTredings(movie.results);
            setIsLoading(false)    
        }
        fetchMovie();    

        return  () => { clearInterval(delay) }
    },[])
    
    const scrollSection = (testando:number, index?:number) => {
        ref.current.scrollLeft = testando;
        

        if( typeof index === "number" ){
            setContagem(index);
        }
    }

    return(
        <>  
        <Wrap ref={ref} style={{maxWidth: 1070, height: 300, overflow: "hidden", display: "flex", flexDirection: "row", scrollBehavior: 'smooth'}}>
                {   
                    tredings.map((content) => 
                        <Background key={content.id} hidden={false} style={{height: 300, backgroundImage:` radial-gradient(circle, rgba(0, 0, 0, 0.5) 0%, rgb(0, 0, 0) 180%), url("https://image.tmdb.org/t/p/w1280${content?.backdrop_path}")`}}>
                            {/*@ts-ignore */}
                            <Title style={{color: "#fff", marginTop: 0, width: 1070, fontWeight: 200}}>{content?.title || content?.name}</Title>
                            
                        </Background>
                    )
                }
                <div style={{position: "absolute", marginTop: 272, marginLeft: 234}}>
                {
                    tredings.map((valor, index) => {  
                        teste[index] = index * 1070;
                        return <TrendingButton key={valor.id} onClick={() =>{ 
                            scrollSection(teste[index],index); 
                        }}>  </TrendingButton>
                    })
                }   
                </div>
        </Wrap>
            
        </>
    )
}

export default Trending;