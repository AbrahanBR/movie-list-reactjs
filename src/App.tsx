import React from 'react';
import { Routes, Route } from 'react-router-dom';
import ActorSection from './components/actorSection';
import Header from './components/header';
import Home from './components/home';
import MoviePage from './components/moviePage';
import SearchedMovies from './components/searchedMovies/idex';
import Trending from './components/trending';

function App() {
  return (
    <>
      <div style={{height: 50, backgroundImage: "linear-gradient(to right, #032541 , #07bbd7, #19d0b3)"}}></div>
      <Header /> 
      <Routes>
        <Route path="/home" element={<Home/>}/>
        <Route path="/movie/:id" element={<MoviePage/>}/>
        <Route path="/tv/:id" element={<MoviePage/>}/>

        <Route path="/search/:id" element={<SearchedMovies/>}/>

        <Route
          path="/"
          element={
            <main style={{ marginTop: "1rem", display:"flex",justifyContent:'center' }}>
              <h1>Oh No. Something Goes Wrong</h1>
            </main>
          }
        />
      </Routes>
    </>
  );
}

export default App;
